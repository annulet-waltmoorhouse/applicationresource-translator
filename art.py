#!/usr/local/bin/python2.7
# encoding: utf-8
'''
art -- ApplicationResources Translator

art is a Python module to translate your ApplicationResources.properties file into other languages.
it uses the goslate library found here: https://bitbucket.org/zhuoqiang/goslate

@author:     waltmoorhouse

@copyright:  2015 Annulet Consulting, LLC. All rights reserved.

@license:   Licensed under the Apache License 2.0
            http://www.apache.org/licenses/LICENSE-2.0

            Distributed on an "AS IS" basis without warranties or conditions of any kind, either express or implied.
'''

import sys
import os
import codecs
import goslate

__all__ = []
__version__ = 0.1
__date__ = '2015-07-25'
__updated__ = '2015-07-25'

DEBUG = 0
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def translate(infile=None, outfile=None, lang=None, lineEnding=None, codec=None):
    gs = goslate.Goslate()
    infileObj = open(infile, "r")
    outfileObj = codecs.open(outfile+"."+codec, "w+", codec)
    for line in infileObj.readlines():
        if ('=' not in line):
            outfileObj.write(line) # should get comments and skipped lines denoting sections
            continue
        lineval = line[line.index('=')+1:].replace('\n', '').replace('\r', '')
        linearg = line[0:line.index('=')]
        translated = gs.translate(lineval, lang)
        outfileObj.write(linearg+"= "+translated+lineEnding)
    infileObj.close()
    outfileObj.close()
    os.system("native2ascii -encoding "+codec+" "+outfile+"."+codec+" "+outfile) # so it will display on the web correctly
    return infile+" translated to "+outfile

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])

    try:
        filename = "./ApplicationResources.properties"
        lineEnding = '\n'
        codec = 'utf-8'
        langs = []
        codecFlag = False
        filenameFlag = False
        lineEndingFlag = False
        for arg in argv:
            if arg == argv[0]:
                continue
            elif filenameFlag:
                filename = arg
                filenameFlag = False
            elif lineEndingFlag:
                lineEnding = arg
                lineEndingFlag = False
            elif codecFlag:
                codec = arg
                codecFlag = False
            elif arg[0] == '-':
                if arg == "-l":
                    lineEndingFlag = True
                elif arg == "-f":
                    filenameFlag = True
                elif arg == "-e":
                    codecFlag = True
            else:
                langs.append(arg)
        
        #check filename end in .properties
        if not filename.endswith('.properties'):
            print("filename does not end in .properties")
            return 2
        
        for lang in langs:
            ### do something with lang ###
            outfile = filename[0:filename.find('.properties')]+'_'+lang+'.properties'
            print(translate(filename, outfile, lang, lineEnding, codec))
        
        print("job complete!")
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        if DEBUG or TESTRUN:
            raise(e)
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'art_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())