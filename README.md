art is a Python module to translate your ApplicationResources.properties file into other languages. It uses the goslate library found here: https://bitbucket.org/zhuoqiang/goslate so you will need to download it and add it to your python path or put it in the same directory as you put the art.py file.

If you are creating a multi-lingual Java website using I18N standards, you will have an ApplicationResources.properties file that contains all the English strings for your application.  If you have programmed it correctly, then adding an ApplicationResources_es.properties file with the same keys but Spanish translations will enable your site to be presented in Spanish.  This script will create those files for you

USAGE:

python art.py [-f ./ApplicationResources.properties] [-l \n] [-e utf-8] lang1 [lang2, lang3 ...]

-f: the path to your .properties file.

-l: the line ending.

-e: the output file encoding.


lang: language code(s) to be translated to.  Check https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes for a list of language codes.